# A catalogue of open NASA blade models

## About

This catalogue contains computer-Aided Design (CAD) models, finite element (FE) meshes and 
reduced-order models (ROM) related to 39 open NASA geometries. Most of these geometries were initially defined at NASA 
Lewis Research Center in the context of research programs conducted in the 70’s and 80’s.
Models related to these geometries have been recently computed and are made publicly available in this catalogue to facilitate 
the comparison of numerical strategies focusing on nonlinear structural interactions.


### Authors
- Copyright (c) 2023 Solène Kojtych (solene.kojtych@polymtl.ca)
- Copyright (c) 2023 Alain Batailly (alain.batailly@polymtl.ca)


### How to cite?
This catalogue should be cited as follows:

 Kojtych, S. and Batailly, A. (2023) 
"A catalogue of open NASA blade models", available at [hal.archives-ouvertes](https://hal.archives-ouvertes.fr)

### License
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

For details on license and warranty, see http://www.gnu.org/licenses
or LICENSE.txt.

## Content 

For each NASA geometry, two blades are considered: 
- the _reference blade_ is defined with multiple-circular arc profiles [1] given in original NASA reports. Corresponding 
models are computed with the open-source code OpenMCAD [2].
- the _initial blade_ is defined with in-house parameters [3] computed from the reference blade CAD model. The geometry of the initial blade is similar to the one of the reference blade.

The following files are provided for each NASA geometry:
* **reference blade** (original multiple-circular arc profiles parameterization [1])
    * CAD model (.step)
    * finite element mesh (.unv and .med) - quadratic pentahedron elements

* **initial blade** (LAVA parameterization [3])
    * CAD model (.step)
    * finite element mesh (.unv and .med) - quadratic pentahedron elements
    * coordinates of boundary nodes (.txt) used for reduced order model
    * reduced order model (.mat)
    
All files are provided in SI units.

Additional détails about the coordinate system and the generation of the reduced order model are provided in README in blade subfolders. 

 
## References
- [1] J. E. Crouse, D. C. Janetzke, R. E. Schwirian, A computer program for composing compressor blading from simulated circular-arc elements on conical surfaces, 1969, NASA Lewis Research Center Cleveland, OH, United States, https://ntrs.nasa.gov/citations/19690027504
- [2] S. Kojtych, A. Batailly (2022) “OpenMCAD, an open blade generator: from Multiple-Circular-Arc profiles to Computer-Aided Design model” [source code], available at [hal.archives-ouvertes](https://hal.science/hal-03923093)
- [3] S. Kojtych, F. Nyssen, C. Audet, A. Batailly, Methodology for the Redesign of Compressor Blades Undergoing Nonlinear Structural Interactions: Application to Blade-tip/casing Contacts. ASME. J. Eng. Gas Turbines Power. 2022, https://hal.archives-ouvertes.fr/hal-03795257 - https://doi.org/10.1115/1.4055681
