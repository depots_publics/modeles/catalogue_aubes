# NASA rotor 18 blade model

## Key characteristics
- nominal speed: $`\Omega_\text{n}=1686.0`$ rad/s
- Craig-Bampton [1,2] reduction parameter: $`\eta=10`$

## Content 
* **reference blade** (original multiple-circular arc profiles parameterization [3])
    * CAD model (.step)
    * finite element mesh (.unv and .med) - quadratic pentahedron elements

* **initial blade** (LAVA parameterization [4])
    * CAD model (.step)
    * finite element mesh (.unv and .med) - quadratic pentahedron elements
    * coordinates of boundary nodes (.txt) used for reduced order model
    * reduced order model (.mat)

All files are provided in SI units.

## Details

* The reference blade is defined with multiple-circular arc profiles given in original NASA report 
(see [additional details](#additional-details)) and computed with an open-source code OpenMCAD [5].

* The initial blade is defined with in-house parameters [4] computed from the reference blade CAD model. It is similar to the reference blade.


### Coordinate system

For all provided models: 
 - the z-axis corresponds to the rotation axis, positive z-direction is from upstream to downstream,
 - the x-axis is parallel to the radial line which passes through a reference point of the blade hub, positive x-direction is from the hub to the top of the blade,
 - the y-axis corresponds to the direction of rotation.
 
### Reduced order model
- blade root is clamped
- stiffness matrix (0 rad/s): $`\mathbf{k}`$
- mass matrix: $`\mathbf{m}`$
- organization of dof [6]: 
    - first 24 dof: boundary nodes along blade tip ($`1,2,3 = x,y,z`$ leading edge... $`22,23,24 = x,y,z`$ trailing edge)
    - $`\eta`$ remaining dof: $\modal dof related to Craig-Bampton reduction
- centrifugal effects not taken into account
- material: 18-Ni-200-maraging steel
    - Young's modulus: 108 GPa
    - Poisson's ratio: 0.34 
    - density: 4400 kg/m3


### Additional details 

https://lava-wiki.meca.polymtl.ca/public/modeles/rotor_18/

## References
- [1] R.-R. Craig, M. C. C. Bampton, Coupling of Substructures for Dynamics Analyses, AIAA Journal. 1968, 6(7), pp. 1313–1319, https://hal.archives-ouvertes.fr/hal-01537654/document - https://arc.aiaa.org/doi/10.2514/3.4741
- [2] A. Batailly, M. Legrand, A. Millecamps, F. Garcin, Numerical-experimental comparison in the simulation of rotor/stator interaction through blade-tip/abradable coating contact, Journal of Engineering for Gas Turbines and Power 134 (8), 2012, 082504–01–11, https://hal.archives-ouvertes.fr/hal-00746632 - https://dx.doi.org/10.1115/1.4006446
- [3] J. E. Crouse, D. C. Janetzke, R. E. Schwirian, A computer program for composing compressor blading from simulated circular-arc elements on conical surfaces, 1969, NASA Lewis Research Center Cleveland, OH, United States, https://ntrs.nasa.gov/citations/19690027504
- [4] S. Kojtych, F. Nyssen, C. Audet, A. Batailly, Methodology for the Redesign of Compressor Blades Undergoing Nonlinear Structural Interactions: Application to Blade-tip/casing Contacts. ASME. J. Eng. Gas Turbines Power. 2022, https://hal.archives-ouvertes.fr/hal-03795257 - https://doi.org/10.1115/1.4055681
- [5] S. Kojtych, A. Batailly (2022) “OpenMCAD, an open blade generator: from Multiple-Circular-Arc profiles to Computer-Aided Design model” [source code], available at [hal.archives-ouvertes](https://hal.science/hal-03923093)
- [6] M. Legrand, A. Batailly, B. Magnain, P. Cartraud, C. Pierre, Full three-dimensional investigation of structural contact interactions in turbomachines, Journal of Sound and Vibration, 331 (11), 2012, pp. 2578–2601, https://hal.archives-ouvertes.fr/hal-00660863v3 - https://doi.org/10.1016/j.jsv.2012.01.017
